package info.androidhive.customlistviewvolley;


import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;


public class ListPressedMenuDialog extends Dialog {

    private RelativeLayout mMakeCall;
    private RelativeLayout mSendEmail;
    private RelativeLayout mCancel;

    String MobileNumber;


    public ListPressedMenuDialog(Activity context) {
        super(context);
        /*
         * used to set dialog to the right of screen
		 */
        Window activityWindow = getWindow();
        WindowManager.LayoutParams windowLayoutParameters = activityWindow
                .getAttributes();
        windowLayoutParameters.gravity = Gravity.RIGHT;
        activityWindow.setAttributes(windowLayoutParameters);

        getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_view_pressed_option);

        mMakeCall = (RelativeLayout) findViewById(R.id.make_call_all);
        mMakeCall.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // make call intent

                SharedPreferencesClass StoreMobileNumber = new SharedPreferencesClass(getContext());
                MobileNumber = StoreMobileNumber.getString("MobileNumber");

             //   String PhoneNum = "01711084352";
                Intent callIntent = new Intent(Intent.ACTION_DIAL);
                callIntent.setData(Uri.parse("tel:" + Uri.encode(MobileNumber.trim())));
                callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getContext().startActivity(callIntent);

                dismiss();
            }
        });

        mSendEmail = (RelativeLayout) findViewById(R.id.make_email_all);
        mSendEmail.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // send email intent


                Intent emailIntent = new Intent(getContext(), EmailActivity.class);
                getContext().startActivity(emailIntent);

                dismiss();
            }
        });

        mCancel = (RelativeLayout) findViewById(R.id.cancel_all);
        mCancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // cancel
                dismiss();
            }
        });

    }


    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        // super.onBackPressed();
    }
}
package info.androidhive.customlistviewvolley;

import info.androidhive.customlistviewvolley.adater.CustomListAdapter;
import info.androidhive.customlistviewvolley.app.AppController;
import info.androidhive.customlistviewvolley.model.Branch;


import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;

public class MainActivity extends Activity {

    private static final String url = "http://192.168.0.142/sbl_api/api/getEBLPhoneBook";
    private ProgressDialog pDialog;
    private List<Branch> branchNameList = new ArrayList<Branch>();
    private ListView listView;
    private CustomListAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = (ListView) findViewById(R.id.list);
        adapter = new CustomListAdapter(this, branchNameList);
        listView.setAdapter(adapter);

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.show();

        makeJsonObjectRequest();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                            @Override
                                            public void onItemClick(AdapterView<?> a, View v, int position,
                                                                    long id) {

                                                Branch item = (Branch) adapter.getItem(position);

                                                String branch_name = item.getBranchName();
                                                Log.e("OnClick", "branch_name:" + branch_name);

                                                String manager_name = item.getManagerName();
                                                Log.e("OnClick", "manager_name:" + manager_name);

                                                String mobile = item.getMobile();
                                                Log.e("OnClick", "MobileNo:" + mobile);

                                                SharedPreferencesClass StoreMobileNumber = new SharedPreferencesClass(MainActivity.this);
                                                StoreMobileNumber.putString("MobileNumber", mobile);

                                                String email = item.getEmail();
                                                Log.e("OnClick", "email:" + email);

                                                SharedPreferencesClass StoreEmailAddress = new SharedPreferencesClass(MainActivity.this);
                                                StoreEmailAddress.putString("EmailAddress", email);


                                                ListPressedMenuDialog listPressedMenuDialog =
                                                        new ListPressedMenuDialog(MainActivity.this);
                                                listPressedMenuDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                                listPressedMenuDialog.setCanceledOnTouchOutside(true);
                                                listPressedMenuDialog.show();

                                                WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
                                                layoutParams.copyFrom(listPressedMenuDialog.getWindow()
                                                        .getAttributes());
                                                layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
                                                listPressedMenuDialog.getWindow().setAttributes(
                                                        layoutParams);
                                                //   return true;

                                            }
                                        }

        );
    }

    private void makeJsonObjectRequest() {

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d("Volley", response.toString());

                try {
                    JSONArray phoneBookJSONArray = response.getJSONArray("phonebook");
                    Log.e("Response", response.toString());


                    for (int i = 0; i < phoneBookJSONArray.length(); i++) {

                        JSONObject obj = phoneBookJSONArray.getJSONObject(i);

                        Branch branch = new Branch();

                        branch.setBranchName(obj.getString("branchName"));
                        Log.e("BranchName: ", branch.getBranchName().toString());

                        branch.setManagerName(obj.getString("managerName"));
                        Log.e("ManagerName: ", branch.getManagerName().toString());

                        branch.setMobile(obj.getString("mobile"));
                        Log.e("Mobile: ", branch.getMobile().toString());

                        branch.setEmail(obj.getString("email"));
                        Log.e("Email", branch.getEmail().toString());

                        // adding movie to movies array
                        branchNameList.add(branch);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("JSONException", e.toString());

                }
                hidePDialog();
                adapter.notifyDataSetChanged();

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Error: ", error.getMessage());
                Log.e("VolleyError: ", error.toString());
                hidePDialog();
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        hidePDialog();
    }

    private void hidePDialog() {
        if (pDialog != null) {
            pDialog.dismiss();
            pDialog = null;
        }
    }

}

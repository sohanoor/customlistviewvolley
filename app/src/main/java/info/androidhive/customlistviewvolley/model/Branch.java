package info.androidhive.customlistviewvolley.model;

import java.util.ArrayList;

public class Branch {
    private String branchName;
    private String managerName;
    private String mobile, email;

    public Branch() {
    }

    public Branch(String name, String managerName, String mobile, String email) {
        this.branchName = name;
        this.managerName = managerName;
        this.mobile = mobile;
        this.email = email;

    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String name) {
        this.branchName = name;
    }

    public String getManagerName() {
        return managerName;
    }

    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


}

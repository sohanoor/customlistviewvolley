package info.androidhive.customlistviewvolley;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import junit.framework.Test;

public class EmailActivity extends Activity {
    EditText send_subject, send_msg_text;
    TextView send_address;
    String to, subject, message;
    Button send_button;

    String EmailAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_email);

        send_address = (TextView) findViewById(R.id.send_address);
        send_subject = (EditText) findViewById(R.id.send_subject);
        send_msg_text = (EditText) findViewById(R.id.send_msg_text);
        send_button = (Button) findViewById(R.id.send_button);

        SharedPreferencesClass StoreEmailAddress = new SharedPreferencesClass(EmailActivity.this);
        EmailAddress = StoreEmailAddress.getString("EmailAddress");

        send_address.setText(EmailAddress);


        send_button.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {



                to = EmailAddress;
                subject = send_subject.getText().toString();
                message = send_msg_text.getText().toString();

                Intent email = new Intent(Intent.ACTION_SEND);
                email.putExtra(Intent.EXTRA_EMAIL, new String[]{to});
                email.putExtra(Intent.EXTRA_SUBJECT, subject);
                email.putExtra(Intent.EXTRA_TEXT, message);
                email.setType("message/rfc822");

                startActivity(Intent.createChooser(email,
                        "Choose an Email client :"));
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {

//			Intent intent = new Intent(getApplicationContext(),
//					MainActivity.class);
//			startActivity(intent);
        finish();

    }
}

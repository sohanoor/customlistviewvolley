package info.androidhive.customlistviewvolley.adater;

import info.androidhive.customlistviewvolley.R;
import info.androidhive.customlistviewvolley.app.AppController;
import info.androidhive.customlistviewvolley.model.Branch;


import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;

public class CustomListAdapter extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<Branch> branchItem;

    public CustomListAdapter(Activity activity, List<Branch> branchItem) {
        this.activity = activity;
        this.branchItem = branchItem;
    }

    @Override
    public int getCount() {
        return branchItem.size();
    }

    @Override
    public Object getItem(int location) {
        return branchItem.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.list_row, null);


        TextView branch_name = (TextView) convertView.findViewById(R.id.branch_name);
        TextView manager_name = (TextView) convertView.findViewById(R.id.manager_name);

        Branch m = branchItem.get(position);


        // branch name
        branch_name.setText(m.getBranchName());

        // manager name
        manager_name.setText(m.getManagerName());


        return convertView;
    }

}